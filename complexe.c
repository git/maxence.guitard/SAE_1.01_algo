/**
 * \file sae.c
 * \brief Ce fichier contient des fonctions afin de gérer un complexe sportif et relaxant.
 * \author ROUX Denovan, GUITARD Maxence, LOUVET Titouan
 * \date 10 novembre 2022
 */


#include "complexe.h"



/**
 * \brief Cette fonction récupère les données stockées dans le fichier adherent.txt, elle les stockes dans leurs tableaux respectifs.
 * \param tabId Tableau contenant l'id des adhérents.
 * \param tabNbP Tableau contenant le nombre de points des adhérents.
 * \param tabEtat Tableau contenant l'état des cartes des adhérents.
 * \param tabDate Tableau contenant la dernière date de fréquentation des adhérents.
 * \param tabPBonus Tableau contenant le nombre de fréquentation des adhérents.
 * \param tmax Taille physique des tableaux
 * \return Taille logique des tableaux
 */

int chargement(int tabID[], int tabNbP[], int tabEtat[], int tabDate[], int tabPBonus[], int tmax)
{
    int i=0,nbp, etat, date, pbonus, id;
    FILE *flot;
    flot = fopen("adherent.txt","r");
    if(flot==NULL)
    {
        printf("Pb d'ouverture du fichier adherent.txt\n");
        fclose(flot);
        return -1;
    }
    fscanf(flot,"%d%d%d%d%d", &id, &nbp, &etat, &date, &pbonus);
    while(!feof(flot))
    {
        if(i==tmax)
        {
            printf("tableaux pleins\n");
            return tmax;
        }
        tabID[i] = id;
        tabNbP[i]=nbp;
        tabEtat[i]=etat;
        tabDate[i]=date;
        tabPBonus[i]=pbonus;
        i=i+1;
        fscanf(flot,"%d%d%d%d%d", &id, &nbp, &etat, &date, &pbonus);
    }
    fclose(flot);
	return i;
}



/**
 * \brief Cette fonction cherche la position dans l'ordre croissant
 * \param tab Tableau trié où l'on cherche 
 * \param val Valeur recherché dans le tableau
 * \param nb Taille logique du tableau
 * \param trouve Pointeur d'un variable pour savoir si val a était trouvé
 * \return Renvoie la position de val si elle est trouvé ou la position où elle doit être insérer
 */
int position(int tab[], int val, int nb, int *trouve)
{
	int i;
	for(i=0; i<nb; i++)
	{
		if(tab[i] == val)
		{
			*trouve = 1;
			return i;
		}
		if(tab[i] > val)
		{
			*trouve = -1;
			return i;
		}
	} 
	*trouve = -1;
	return nb;
}


/**
 * \brief Cette fonction sauvegarde les données changés dans le fichier adherent.txt
 * \param tabID Les éléments de tabID seront enregistrés dans le fichier
 * \param tabNbP Les éléments de tabNbP seront enregistrés dans le fichier
 * \param tabEtat Les éléments de tabEtat seront enregistrés dans le fichier
 * \param tabDate Les éléments de tabDate seront enregistrés dans le fichier
 * \param tabPBonus Les éléments de tabPBonus seront enregistrés dans le fichier
 * \param nb Taille logique des tableaux 
 */


void sauvegarde(int tabID[], int tabNbP[], int tabEtat[],  int tabDate[], int tabPBonus[], int nb)
{
	FILE *flot;
	int i=0;
	flot = fopen("adherent.txt", "w");
	if (flot == NULL)
	{
		printf("Erreur d'ouverture du fichier matieres_save.txt en écriture\n");
		fclose(flot);
		return;
	}
	while(i<nb)
	{
		fprintf(flot, "%d\t%d\t%d\t%d\t%d\n",tabID[i], tabNbP[i], tabEtat[i], tabDate[i], tabPBonus[i]);
		i++;
	}
	fclose(flot);
}



/**
 * \brief Cette fonction sert a supprimer un adhérent et toutes ses données
 * \param tabID Les éléments concernant l'adhérents de tabID vont etre supprimer
 * \param tabNbP Les éléments concernant l'adhérents de tabNbP vont etre supprimer
 * \param tabEtat Les éléments concernant l'adhérents de tabEtat vont etre supprimer
 * \param tabDate Les éléments concernant l'adhérents de tabDate vont etre supprimer
 * \param tabPBonus Les éléments concernant l'adhérents de tabPBonus vont etre supprimer
 * \param nb On enlève 1 a la taille logique des tableaux
 */ 

void supprAdherent(int tabID[], int tabNbP[], int tabEtat[],  int tabDate[], int tabPBonus[], int *nb)
{
	int i, trouve, id, pos;
	printf("Entrez l'id que vous souhaitez supprimer : ");
	scanf("%d", &id);
	pos = position(tabID, id, *nb, &trouve);
	if(trouve == -1)
	{
		printf("Id non existant");
		return;
	}
	for(i=pos; i<=*nb; i++) // décaler à gauche
	{
		tabID[i] = tabID[i+1];
		tabNbP[i] = tabNbP[i+1];
		tabEtat[i] = tabEtat[i+1];
		tabPBonus[i] = tabPBonus[i+1];
	}
	*nb = *nb-1;
}



/**
 * \brief Cette fonction affiche tous les adhérents avec leur infos
 * \param tabID Affiche les ID
 * \param tabPBonus Affiche les Points bonus
 * \param tabEtat Affiche l'état des cartes
 * \param tabNbP Affiche le nombre de points
 * \param tabDate Affiche la date de la dernière fréquentation
 * \param nb Taille logique des tableaux
 */

void afficherTAdherent(int tabID[], int tabPBonus[], int tabEtat[], int tabNbP[], int tabDate[], int nb)
{	
	int i; 
	printf("N° Adhérent\tNbre de points\tÉtat carte\tNbre points bonus\tDate derniere venue\n");
	for(i=0; i<nb; i++)
		printf("%d\t\t%d\t\t%d\t\t%d\t\t\t%d\n", tabID[i], tabNbP[i], tabEtat[i], tabPBonus[i], tabDate[i]);
}



/**
 * \brief Cette fonction sert a ajouter un adhérent et toutes ses données
 * \param tabID Les éléments concernant l'adhérents de tabID vont etre ajouter
 * \param tabNbP Les éléments concernant l'adhérents de tabNbP vont etre ajouter
 * \param tabEtat Les éléments concernant l'adhérents de tabEtat vont etre ajouter
 * \param tabDate Les éléments concernant l'adhérents de tabDate vont etre ajouter
 * \param tabPBonus Les éléments concernant l'adhérents de tabPBonus vont etre ajouter
 * \param nb On ajoute 1 a la taille logique des tableaux
 */ 

void insertAdherent(int tabID[], int tabNbP[], int tabEtat[], int tabDate[], int tabPBonus[], int *nb, int tPhysiq)
{
 
    int ID, euro, i, verif, pos;
 
    if(*nb == tPhysiq)
    {
        printf("Err : Problème de place, veuillez supprimer des adhérents.\n");
        return;
    }
  
    printf("Entrez l'ID de l'adhérent :");
    scanf("%d", &ID);
    pos = position(tabID, ID, *nb, &verif);

    if(verif == 1)
    {
        printf("Err : L'adhérent existe déjà.\n");
        return;
    }   
    printf("\nEntrez l'argent en € déboursé par l'adhérent à l'inscription (2pt = 1€):");
    scanf("%d", &euro);
 

 
    //Décalage à droite
    for(i=*nb; i>=pos; i--)
    {
        tabID[i+1] = tabID[i];
        tabNbP[i+1] = tabNbP[i];
        tabEtat[i+1] = tabEtat[i];
        tabDate[i+1] = tabDate[i];
        tabPBonus[i+1] = tabPBonus[i];
    }
 
    tabID[pos] = ID;
    tabNbP[pos] = euro * 2;
    tabEtat[pos] = 1;
    tabDate[pos] = 0;
    tabPBonus[pos] = 0;
    *nb = *nb+1;
}




/**
 * \brief Cette fonction est un menu qui affiche les activités et les fait en fonction du choix de l'adhérents
 * \param tabID L'id de l'adhérent est rentré pour utiliser les points sur son compte
 * \param tabDate La date de dernière fréquentation est changé à celle du jour
 * \param tabEtat Vérifie que l'étatt de la carte de l'adhérent est activé
 * \param tabNbP Compte le nm de points de l'ahdérent, en enlève ou en ajoute
 * \param tabPBonus Donne des points à l'ahdérent si il a fait plus de 10 activités
 * \param nb Taille logique du tableau 
 */

void menuActivite(int tabID[], int tabDate[], int tabEtat[], int tabNbP[], int tabPBonus[], int tabActivite[], int nb)
{
	int i, trouve, pos, id, point, date, choix, ptTotal = 0, autreAct, ptCumul;
	printf("Entrez l'ID du compte avec lequel vous souhaitez faire une/des activités : ");
	scanf("%d", &id);
	pos = position(tabID, id, nb, &trouve);
	if(trouve == -1)
	{
		printf("ID non existant\n");
		return;
	}
	printf("Entrez la date du jour (JJMMAAAA) : ");
	scanf("%d", &date);
	if(tabDate[pos] == date)
	{
		printf("Vous avez déja fréquenter le centre aujourd'hui\n");
		return;
	}
	if(tabEtat[pos] == 0)
	{
		printf("Votre carte est désactiver\n");
		return;
	}
    tabDate[pos] = date;
    printf("\nUn système de point bonus est actuellement en cours. Au bout de 10 activités effectué, vous gagnez 20 points.\n");
    printf("Vous avez actuellement effectué %d activités.\n", tabPBonus[pos]);
	choix = afficherActivite();
	ptCumul = tabNbP[pos];
	while((choix >= 1 || choix <= 7) && choix!=9)
    {
        if(choix==1)
        {
        	if(10 > ptCumul)
            {
        		printf("Vous n'avez pas assez de points\n");
                return;
            }
        	else
        	{
        		ptCumul = ptCumul - 10;
        		ptTotal = ptTotal + 10;
                printf("%d\n", ptTotal);
                tabPBonus[pos] = tabPBonus[pos] + 1;
                tabActivite[0] = tabActivite[0] +1;
                if(tabPBonus[pos] >= 10)
                {
                    printf("Vous avez gagné 20points !\n");
                    tabPBonus[pos] = tabPBonus[pos] - 10;
                    ptCumul = ptCumul + 20;
                }
            	printf("Vous avez choisis une scéance de musculation. Souhaitez vous faire une autre activité aprés cette séance ? (Oui :1 / Non: 0): ");
            	scanf("%d", &autreAct);
            	if(autreAct == 0)
            		break;
            	if(autreAct == 1)
            		choix = afficherActivite();
            } 
        }
        if(choix==2)
        {
        	if(15 > ptCumul)
            {
        		printf("Vous n'avez pas assez de points\n");
                return;
            }
        	else
        	{
        		ptCumul = ptCumul - 15;
        		ptTotal = ptTotal + 15;
                printf("%d\n", ptTotal);
                tabPBonus[pos] = tabPBonus[pos] + 1;
                tabActivite[1] = tabActivite[1] +1;
                if(tabPBonus[pos] >= 10)
                {
                    printf("Vous avez gagné 20points !\n");
                    tabPBonus[pos] = tabPBonus[pos] - 10;
                    ptCumul = ptCumul + 20;
                }
            	printf("Vous avez choisis une scéance d'aquagym. Souhaitez vous faire une autre activité aprés cette séance ? (Oui :1 / Non: 0): ");
            	scanf("%d", &autreAct);
            	if(autreAct == 0)
            		break;
            	if(autreAct == 1)
            		choix = afficherActivite();
            } 
        }
        if(choix==3)
        {
        	if(12 > ptCumul)
            {
        		printf("Vous n'avez pas assez de points\n");
                return;
            }
        	else
        	{
        		ptCumul = ptCumul - 12;
        		ptTotal = ptTotal + 12;
                printf("%d\n", ptTotal);
                tabPBonus[pos] = tabPBonus[pos] + 1;
                tabActivite[2] = tabActivite[2] +1;
                if(tabPBonus[pos] >= 10)
                {
                    printf("Vous avez gagné 20points !\n");
                    tabPBonus[pos] = tabPBonus[pos] - 10;
                    ptCumul = ptCumul + 20;
                }
            	printf("Vous avez choisis une scéance de SPA. Souhaitez vous faire une autre activité aprés cette séance ? (Oui :1 / Non: 0): ");
            	scanf("%d", &autreAct);
            	if(autreAct == 0)
            		break;
            	if(autreAct == 1)
            		choix = afficherActivite();	
            } 
        }
        
        if(choix==4)
        {
        	if(ptCumul < 13)
            {
        		printf("Vous n'avez pas assez de points\n");
                return;
            }
        	else
        	{
        		ptCumul = ptCumul - 13;
        		ptTotal = ptTotal + 13;
                printf("%d\n", ptTotal);
                tabPBonus[pos] = tabPBonus[pos] + 1;
                tabActivite[3] = tabActivite[3] +1;
                if(tabPBonus[pos] >= 10)
                {
                    printf("Vous avez gagné 20points !\n");
                    tabPBonus[pos] = tabPBonus[pos] - 10;
                    ptCumul = ptCumul + 20;
                }
            	printf("Vous avez choisis une scéance de badminton. Souhaitez vous faire une autre activité aprés cette séance ? (Oui :1 / Non: 0): ");
            	scanf("%d", &autreAct);
            	if(autreAct == 0)
            		break;
            	if(autreAct == 1)
            		choix = afficherActivite();
            } 
        }
        if(choix==5)
        {
        	if(20 > ptCumul)
            {
        		printf("Vous n'avez pas assez de points\n");
                return;
            }
        	else
        	{
        		ptCumul = ptCumul - 20;
        		ptTotal = ptTotal + 20;
                printf("%d\n", ptTotal);
                tabPBonus[pos] = tabPBonus[pos] + 1;
                tabActivite[4] = tabActivite[4] +1;
                if(tabPBonus[pos] >= 10)
                {
                    printf("Vous avez gagné 20points !\n");
                    tabPBonus[pos] = tabPBonus[pos] - 10;
                    ptCumul = ptCumul + 20;
                }
            	printf("Vous avez choisis une scéance de massage. Souhaitez vous faire une autre activité aprés cette séance ? (Oui :1 / Non: 0): ");
            	scanf("%d", &autreAct);
            	if(autreAct == 0)
            		break;
            	if(autreAct == 1)
            		choix = afficherActivite();
            } 

        }
        if(choix==6)
        {
        	if(11 > ptCumul)
            {
        		printf("Vous n'avez pas assez de points\n");
                return;
            }
        	else
        	{
        		ptCumul = ptCumul - 11;
        		ptTotal = ptTotal + 11;
                printf("%d\n", ptTotal);
                tabPBonus[pos] = tabPBonus[pos] + 1;
                tabActivite[5] = tabActivite[5] +1;
                if(tabPBonus[pos] >= 10)
                {
                    printf("Vous avez gagné 20points !\n");
                    tabPBonus[pos] = tabPBonus[pos] - 10;
                    ptCumul = ptCumul + 20;
                }
            	printf("Vous avez choisis une scéance de relaxation. Souhaitez vous faire une autre activité aprés cette séance ? (Oui :1 / Non: 0): ");
            	scanf("%d", &autreAct);
            	if(autreAct == 0)
            		break;
            	if(autreAct == 1)
            		choix = afficherActivite();
            } 
        }
        if(choix==7)
        {
            printf("\nVous avez actuellement %d points.\n", ptCumul);
            choix = afficherActivite();
        }
        else
        {
            printf("Valeur saisie incorrect, veuillez réessayer\n");
            choix = afficherActivite();
        }
    }
	printf("Vous avez dépenser %d points\n", ptTotal);
	tabNbP[pos] = ptCumul;
}



/**
 * \brief Affiche le menu des activités
 * \return Renvoi le choix de l'adhérents
 */

int afficherActivite(void)
{
	int choix;
    printf("\n1\tSéance de musculation (10pt)\n");
    printf("2\tSéance d'aquagym (15pt)\n");
    printf("3\tSéance de SPA (12pt)\n");
    printf("4\tSéance de Badminton (13pt)\n");
    printf("5\tSéance de massage (20pt)\n");
    printf("6\tSéance de relaxation (11pt)\n");
    printf("7\tAfficher votre nombre de points\n");
    printf("9\tQuitter\n\n");
 	printf("Entrez votre choix : ");
	scanf("%d", &choix);
	return choix;
}   



/**
 * \brief Affiche un adhérent en entrant son ID
 * \param tabID Affiche l'id de l'ahérent et vérifie qu'il y est
 * \param tabNbP Affiche le nombre de points de l'adhérent
 * \param tabEtat Affiche l'étant de la carte de l'adhérent
 * \param tabDate Affiche la date de la dernière fréquentation de l'adhérent
 * \param tabPBonus Affiche les points bonus de l'adhérent
 * \param nb Taille logique des tableaux
 */

void afficherAdherent(int tabID[], int tabNbP[], int tabEtat[], int tabDate[], int tabPBonus[], int nb)
{
 
   int id, pos, verif;
 
   printf("Entrez l'ID de l'adhérent :");
   scanf("%d", &id);
 
   pos = position(tabID, id, nb, &verif);
 
   if(verif == -1)
   {
       printf("Err : L'adhérent ciblé n'existe pas.\n");
       return;
   }
 
   printf("N°Adhérent\tNbre Points\tEtat Carte\tDernière Visite (JJMMYYYY)\n");
   printf("%d\t\t%d\t\t", id, tabNbP[pos]);
   if(tabEtat[pos] == 0)
   {
       printf("Désactivé\t");
   } else {
       printf("Activé\t");
   }
   printf("\t%d\n", tabDate[pos]);
}



/**
 * \brief Cette fonction ajoute des points à un adhérent en entrant son ID
 * \param tabID Le tableau avec les id pour vérifier que l'adhérent est existant 
 * \param tabNbP Ajoute les points à l'adhérent
 * \param nb La taille logique des tableaux
 */  

void ajouterPoint(int tabID[], int tabNbP[], int nb)
{
 
   int id, pos, verif, euro;
 
   printf("Entrez l'ID de l'adhérent :");
   scanf("%d", &id);
 
   pos = position(tabID, id, nb, &verif);
 
   if(verif == -1)
   {
       printf("Err : L'adhérent ciblé n'existe pas.\n");
       return;
   }

   printf("Entrez l'argent en € déboursé par l'adhérent (2pt = 1€):");
   scanf("%d", &euro);

   tabNbP[pos] = tabNbP[pos] + euro*2;
 
}


/**
 * \brief Cette fonction affiche le nombre d'utilisation d'activité par jours
 * \param tabActivite Tableau ou est stocké les utilisations des activité
 */

void activiteJour(int tabActivite[])
{
    printf("\nMusculation: %d\n", tabActivite[0]);
    printf("Aquagym: %d\n", tabActivite[1]);
    printf("SPA: %d\n", tabActivite[2]);
    printf("Badminton: %d\n", tabActivite[3]);
    printf("Massage: %d\n", tabActivite[4]);
    printf("Relaxation: %d\n", tabActivite[5]);
}



/**
 * \brief Cette fonction affiche l'état de la carte d'un adhérent et le change à la demande
 * \param tabID Tableau des id pour vérifier que l'adhérent existe et changer l'état de sa carte
 * \param tabEtat Tableau ou est stocké l'état des cartes
 * \param nb Taille logique des tableaux
 */

void etatcarte(int tabID[], int tabEtat[], int nb)
{
    int etat, pos, id, arret, trouve;
    printf("ID de l'adhérent :");
    scanf("%d",&id);
    pos = position(tabID,id,nb,&trouve);
    while(1)
    {
        if(trouve==1)
        {
            printf("L'état actuel de la carte est : ");
            if(tabEtat[pos] == 1)
                printf("Activé\n");
            else printf("Désactivé\n");

            printf("Activer(1)/Désactiver(0): ");
            scanf("%d",&etat);
            while(etat != 1 && etat != 0)
            {
            	printf("Valeur saisi non correct, veuillez réesayer\n");
            	printf("Activer(1)/Désactiver(0): ");
            	scanf("%d",&etat);
            }
            tabEtat[pos] = etat;
            return;
                        }
        if(trouve==-1)
        {
            printf("Aucun N° d'adhérent avec comme id %d trouvée\n", id);
            printf("Voulez vous ressaisir un nouveau N° d'adhérent(1) OU annulé(0) : ");
            scanf("%d", &arret);
            if(arret == 1)
            {
                printf("Veuillez ressaisir un N° d'adhérent :");
                scanf("%d",&id);
                pos = position(tabID,id,nb,&trouve);
        	}
        	if(arret == 0)
        		return;
        }
    }
}



/**
 * \brief Affiche le menu de gestion 
 * \return le choix entré par l'utilisateur
 */

int choixMenu(void)
{
	int choix;
	printf("\nGestion du complexe\n\n");
	printf("1\tAfficher tous les adhérents\n");
	printf("2\tAfficher un seul adhérent\n");
	printf("3\tCréer un adhérent\n");
	printf("4\tSupprimer un adhérent\n");
	printf("5\tAlimenter une carte\n");
	printf("6\tActiver/Désactiver une carte\n");
	printf("7\tFaire une activité\n");
    printf("8\tAfficher nbres d'entrée par activité\n");
	printf("9\tQuitter\n\n");
	printf("Entrez votre choix : ");
	scanf("%d%*c", &choix);
	return choix;
}

 

/**
 * \brief Fonction qui gère toutes les autres fonctions, les atbleaux sont crées dedans et utilisé pour les appels des fonctions.
 */

void global(void)
{
    char rep='n';
    int tabID[100], tabNbP[100], tabEtat[100], tabDate[100], tabPBonus[100], tabActivite[100]={0};
    int nb, choix;
    nb = chargement(tabID, tabNbP, tabEtat, tabDate, tabPBonus, 100);
    if(nb<0)
        return;
    choix=choixMenu();
    fonction : while(rep=='n'||rep=='N'){
        while(choix!=9)
        {
            switch(choix){
                case 1:

                    afficherTAdherent(tabID, tabPBonus, tabEtat, tabNbP, tabDate, nb);
                    break;;
                case 2:
                    afficherAdherent(tabID, tabNbP, tabEtat, tabDate, tabPBonus, nb);
                    break;
                case 3:
                    insertAdherent(tabID, tabNbP, tabEtat, tabDate, tabPBonus, &nb, 100);
                    break;
                case 4:
                    supprAdherent(tabID, tabNbP, tabEtat, tabDate, tabPBonus, &nb);
                    break;
                
                case 5:
                    ajouterPoint(tabID, tabNbP, nb);
                    break;
                case 6:
                    etatcarte(tabID, tabEtat, nb);
                    break;
                case 7:
                    menuActivite(tabID, tabDate, tabEtat, tabNbP, tabPBonus, tabActivite, nb);
                    break;
                case 8:
                    activiteJour(tabActivite);
                    break;
                default:
                    printf("Valeur saisie incorrect, veuillez réessayer\n");
                    break;
            }
            
            choix = choixMenu();
        }
        printf("Êtes vous sur de vouloir quitter ? (O/N): ");
        scanf("%c%*c",&rep);
        while((rep != 'o' && rep != 'O' && rep != 'n' && rep != 'N') ){
            printf("Erreur saisie, Êtes vous sur de vouloir quitter ? (O/N): ");
            scanf("%c%*c",&rep);
        }

        if(rep=='o' || rep=='O'){
            sauvegarde(tabID, tabNbP, tabEtat, tabDate, tabPBonus, nb);
            printf("Les données sont sauvegardées\nAu revoir ;)\n");
            break;
        }
        choix = choixMenu();
        goto fonction;
    } 
}