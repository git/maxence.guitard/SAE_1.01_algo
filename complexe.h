#include <stdio.h>

int chargement(int tabID[], int tabNbP[], int tabEtat[], int tabDate[], int tabPBonus[], int tmax);

int choixMenu(void);

int position(int tab[], int val, int nb, int *trouve);

void sauvegarde(int tabID[], int tabNbP[], int tabEtat[],  int tabDate[], int tabPBonus[], int nb);

void supprAdherent(int tabID[], int tabNbP[], int tabEtat[],  int tabDate[], int tabPBonus[], int *nb);

void menuActivite(int tabID[], int tabDate[], int tabEtat[], int tabNbP[], int tabPBonus[], int tabActivite[], int nb);

void etatcarte(int tabID[], int tabEtat[], int nb);

int afficherActivite(void);

void activiteJour(int tabActivite[]);

void ajouterPoint(int tabID[], int tabNbP[], int nb);

void insertAdherent(int tabID[], int tabNbP[], int tabEtat[], int tabDate[], int tabPBonus[], int *nb, int tPhysiq);

void afficherAdherent(int tabID[], int tabNbP[], int tabEtat[], int tabDate[], int tabPBonus[], int nb);

void afficherTAdherent(int tabID[], int tabPBonus[], int tabEtat[], int tabNbP[], int tabDate[], int nb);

void global(void);